//
//  ViewController.m
//  SQLiteImporter
//
//  Created by Roman Sinelnikov on 14/05/16.
//  Copyright © 2016 eqhako. All rights reserved.
//

#import "ViewController.h"
#import <CHCSVParser/CHCSVParser.h>
#import <FMDB/FMDB.h>
@interface ViewController()
<NSOpenSavePanelDelegate>
@property (nonatomic, strong) FMDatabase * database;
@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.database = [[FMDatabase alloc] initWithPath:@"import.db"];
    if (![self.database open]) {
        self.database = nil;
        NSAlert * alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        alert.messageText = @"Can't open database";
        [alert runModal];
    }
    NSString * create
     = @"create table if not exists data (id integer primary key autoincrement, timestamp text,"
     @" session text, event text, description text, version text, platform text, device text,"
     @" user text, params text)";
    
    NSString * uniqueIndex
    = @"create unique index if not exists uniq_idx on data(params, timestamp, session, event)";
    
    [self.database executeStatements:create];
    [self.database executeStatements:uniqueIndex];
    
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
}

- (void)importCsv:(NSURL *)csvURL {
    NSArray * array = [NSArray arrayWithContentsOfCSVURL:csvURL
                                                 options:CHCSVParserOptionsUsesFirstLineAsKeys];
    
    NSError * error = nil;
    BOOL success = YES;
    for (NSDictionary * obj in array) {
        //Timestamp,Session Index,Event,Description,Version,Platform,Device,User ID,Params
        NSString * timestamp = obj[@"Timestamp"];
        NSString * sessionIndex = obj[@"Session Index"];
        NSString * event = obj[@"Event"];
        NSString * description = obj[@"Description"];
        NSString * version = obj[@"Version"];
        NSString * platform = obj[@"Platform"];
        NSString * device = obj[@"Device"];
        NSString * userId = obj[@"User ID"];
        NSString * params = obj[@"Params"];
        
        
        success =
        [self.database executeUpdate:@"insert or replace into data values(NULL,?,?,?,?,?,?,?,?,?)"
                              values:@[
                                       timestamp,
                                       sessionIndex,
                                       event,
                                       description,
                                       version,
                                       platform,
                                       device,
                                       userId,
                                       params]
                               error:&error
         
         
         ];
        
         if (!success) {
            break;
         }
        
    }
    
    NSAlert * alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"OK"];
    if (!success) {
        alert.messageText = [NSString stringWithFormat:@"Error: %@", error.localizedDescription];
    } else {
        alert.messageText = @"Import successfull";
    }
    
    [alert runModal];

    
}

- (IBAction)chooseFileButtonDidClick:(id)sender {
    NSOpenPanel *panel = [NSOpenPanel openPanel];
    panel.canChooseDirectories = NO;
    panel.allowsMultipleSelection = NO;
    panel.canChooseFiles = YES;
    panel.delegate = self;
    
    [panel beginWithCompletionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
            NSURL * doc = [[panel URLs] objectAtIndex:0];
            [self importCsv:doc];
        }
    }];
}

#pragma mark - NSOpenSavePanelDelegate

- (BOOL)panel:(id)sender shouldEnableURL:(NSURL *)url {
    NSString* ext = [[url path] pathExtension];
    if ([ext  isEqual: @""] || [ext  isEqual: @"/"] || ext == nil || ext == NULL || [ext length] < 1) {
        return TRUE;
    }
    
    NSLog(@"Ext: '%@'", ext);
    
    NSEnumerator* tagEnumerator = [[NSArray arrayWithObjects:@"csv", nil] objectEnumerator];
    NSString* allowedExt;
    while ((allowedExt = [tagEnumerator nextObject]))
    {
        if ([ext caseInsensitiveCompare:allowedExt] == NSOrderedSame)
        {
            return TRUE;
        }
    }
    
    return FALSE;
}


@end
