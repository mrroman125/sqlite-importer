//
//  AppDelegate.h
//  SQLiteImporter
//
//  Created by Roman Sinelnikov on 14/05/16.
//  Copyright © 2016 eqhako. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

