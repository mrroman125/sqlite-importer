//
//  main.m
//  SQLiteImporter
//
//  Created by Roman Sinelnikov on 14/05/16.
//  Copyright © 2016 eqhako. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
