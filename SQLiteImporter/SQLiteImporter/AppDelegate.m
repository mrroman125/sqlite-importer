//
//  AppDelegate.m
//  SQLiteImporter
//
//  Created by Roman Sinelnikov on 14/05/16.
//  Copyright © 2016 eqhako. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
